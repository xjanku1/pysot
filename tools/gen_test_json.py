# Author: Kristyna Janku

import json
from os import path

dataset_name = 'Fluo-N2DH-SIM+'
vid = '01'
annotation_json_path = f'../training_dataset/{dataset_name}/test.json'
test_json_path = path.join(path.expanduser('~'), f'data/{dataset_name}/annotation/SOT/{dataset_name}.json')
numbers_in_name = 3

test_json = {}

with open(annotation_json_path, 'r') as annotation_file:
    annotations = json.load(annotation_file)
    for obj, obj_annotations in annotations[vid].items():
        seq = f'{vid}_{obj}'
        test_json[seq] = {
            'video_dir': vid,
            'init_rect': [],
            'img_names': [],
            'gt_rect': []
        }
        for frame, bbox in obj_annotations.items():
            if len(test_json[seq]['init_rect']) == 0:
                test_json[seq]['init_rect'] = [bbox[0], bbox[1], bbox[2]-bbox[0], bbox[3]-bbox[1]]
            test_json[seq]['img_names'].append(path.join(vid, f't{frame[(6-numbers_in_name):]}.png'))
            test_json[seq]['gt_rect'].append([bbox[0], bbox[1], bbox[2]-bbox[0], bbox[3]-bbox[1]])

with open(test_json_path, 'w') as test_json_file:
    json.dump(test_json, test_json_file, indent=4)
