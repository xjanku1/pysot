# Author: Kristyna Janku, Snippets creation and structure inspired by gen_json.py for VID dataset

import os
import glob
import json
import cv2
import numpy as np


def get_bounding_box(mask, padding=5, min_area=6):
    # Get bounding box of object from maximal contour
    contours = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[0]
    areas = [cv2.contourArea(c) for c in contours]
    max_index = np.argmax(areas)
    max_contour = contours[max_index]
    x, y, width, height = cv2.boundingRect(max_contour)

    if (width * height) < min_area:
        return 0, 0, 0, 0

    im_height, im_width = mask.shape

    # Add padding
    x1 = max(x - padding, 0)
    y1 = max(y - padding, 0)
    x2 = min(x + width + padding, im_width)
    y2 = min(y + height + padding, im_height)
    width = x2 - x1
    height = y2 - y1

    return x1, y1, width, height


if __name__ == '__main__':
    root_data_path = os.path.join(os.path.expanduser('~'), 'data/Fluo-N2DH-SIM+/raw_data')
    videos = {'01': 'test', '02': 'train'}

    snippets = dict()
    num_videos = 0

    for video in videos:
        id_set = []
        id_frames = {}
        snippets[video] = dict()

        subset = videos[video]
        data_path = os.path.join(root_data_path, subset)

        # Load numbers of all frames from filenames
        frames = sorted([''.join(filter(str.isdigit, frame)) for frame in os.listdir(os.path.join(data_path, video)) if frame.endswith('.png')])

        # Load tracking file and get information about objects and frames they appear in
        with open(os.path.join(data_path, video + '_GT', 'TRA', 'man_track.txt'), 'r') as track_file:
            for line in track_file:
                parts = line.split(' ')
                obj_id = parts[0]
                id_set.append(int(obj_id))
                id_frames[int(obj_id)] = list(range(int(parts[1]), (int(parts[2]) + 1)))
                snippets[video][obj_id.zfill(2)] = dict()

        for frame in frames:
            # Load segmented image
            seg = glob.glob(os.path.join(data_path, video + '_GT', 'SEG', '*' + frame + '*'))
            seg_image = cv2.imread(seg[0], cv2.IMREAD_ANYDEPTH)

            for obj_id in range(1, seg_image.max() + 1):
                if obj_id in id_set and int(frame) in id_frames[obj_id]:
                    # Mask only pixels with current object, multiply by 1 to change True/False to 1/0
                    mask = (seg_image == obj_id) * 1
                    # Convert to 8-bit image to enable further transformations
                    mask = cv2.convertScaleAbs(mask)
                    # Checking if tracked object really is in segmented image
                    if mask.max() == 0:
                        print(f'Video: {video} Frame: {frame} Missing object id in seg: {str(obj_id)}')
                    else:
                        x, y, width, height = get_bounding_box(mask, padding=5, min_area=6)
                        area = width * height

                        # Ignore annotations with area less than min_area (width, height set to 0 from get_bounding_box)
                        if area > 0:
                            snippets[video][str(obj_id).zfill(2)][frame.zfill(6)] = [x, y, x + width - 1, y + height - 1]

        print('Video number: {:s} Snippets count: {:d}'.format(video, len(snippets[video])))

    # Divide into training and validation data
    train = {k: v for (k, v) in snippets.items() if '02' in k}
    test = {k: v for (k, v) in snippets.items() if '01' in k}
    all = {k: v for (k, v) in snippets.items()}

    # Save to json files
    json.dump(train, open('train.json', 'w'), indent=4, sort_keys=True)
    json.dump(test, open('test.json', 'w'), indent=4, sort_keys=True)
    json.dump(all, open('all.json', 'w'), indent=4, sort_keys=True)
    print('All videos done!')
