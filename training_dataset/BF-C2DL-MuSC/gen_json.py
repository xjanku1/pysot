# Author: Kristyna Janku, Snippets structure inspired by gen_json.py for VID dataset
import json
import cv2
import glob
import numpy as np
from os import listdir, path
from numba import njit


def get_bounding_box(mask, padding=5, min_area=6):
    # Get bounding box of object from maximal contour
    contours = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[0]
    areas = [cv2.contourArea(c) for c in contours]
    max_index = np.argmax(areas)
    max_contour = contours[max_index]
    x, y, width, height = cv2.boundingRect(max_contour)

    if (width * height) < min_area:
        return 0, 0, 0, 0

    im_height, im_width = mask.shape

    # Add padding
    x1 = max(x - padding, 0)
    y1 = max(y - padding, 0)
    x2 = min(x + width + padding, im_width)
    y2 = min(y + height + padding, im_height)
    width = x2 - x1
    height = y2 - y1

    return x1, y1, width, height


@njit
def tracking_to_segmentation_labels(tracked_ids, track_image, seg_image):
    track_to_seg_labels = {}

    # Look for tracking markers in tracking image
    for i in range(track_image.shape[0]):
        for j in range(track_image.shape[1]):
            track_id = track_image[i, j]
            if track_id in tracked_ids:
                # Get segmentation label of tracked object
                seg_label = seg_image[i, j]
                if seg_label > 0 and track_id not in track_to_seg_labels:
                    track_to_seg_labels[track_id] = seg_label
                    if len(tracked_ids) == len(track_to_seg_labels):
                        return track_to_seg_labels

    return track_to_seg_labels


def get_annotations(videos_folder, video, snippets):
    snippets[video] = {}

    gt_annotation_path = path.join(videos_folder, f'{video}_GT')
    gt_segmentation_folder_path = path.join(gt_annotation_path, 'SEG')
    gt_tracking_folder_path = path.join(gt_annotation_path, 'TRA')

    st_segmentation_folder_path = path.join(videos_folder, f'{video}_ST', 'SEG')

    # Load numbers of all frames from filenames (only PNG files)
    frames = sorted([''.join(filter(str.isdigit, frame)) for frame
                     in listdir(path.join(videos_folder, video)) if frame.endswith('.png')])

    for frame in frames:

        # Load segmented image, gold truth if available, silver truth otherwise
        seg = glob.glob(path.join(gt_segmentation_folder_path, f'*{frame}*'))
        if len(seg) == 0:
            seg = glob.glob(path.join(st_segmentation_folder_path, f'*{frame}*'))
        seg_image = cv2.imread(seg[0], cv2.IMREAD_ANYDEPTH)

        # Load tracking image with object markers
        track = glob.glob(path.join(gt_tracking_folder_path, f'*{frame}*'))
        track_image = cv2.imread(track[0], cv2.IMREAD_ANYDEPTH)

        # Find tracked ids and map them to segmentation labels
        tracked_ids = np.unique(track_image)[1:]
        track_to_seg_labels = tracking_to_segmentation_labels(tracked_ids, track_image, seg_image)

        # Check if segmentation contains all tracked objects
        len_diff = len(tracked_ids) - len(track_to_seg_labels)
        if len_diff > 0:
            print(f'Video: {video} Frame: {frame} Missing {len_diff} tracked objects in seg')

        for obj_id in track_to_seg_labels:
            # Mask only pixels with current object, multiply by 1 to change True/False to 1/0
            mask = (seg_image == track_to_seg_labels[obj_id]) * 1

            # Convert to 8-bit image to enable further transformations
            mask = cv2.convertScaleAbs(mask)

            x, y, width, height = get_bounding_box(mask, padding=5, min_area=6)
            area = width * height

            # Ignore annotations with area less than min_area (width, height set to 0 from get_bounding_box)
            if area > 0:
                aligned_obj_id = str(obj_id).zfill(3)
                if aligned_obj_id not in snippets[video]:
                    snippets[video][aligned_obj_id] = {}
                snippets[video][aligned_obj_id][frame.zfill(6)] = [x, y, x + width - 1, y + height - 1]

    return snippets


if __name__ == '__main__':
    dataset_name = 'BF-C2DL-MuSC'
    data_path = path.join(path.expanduser('~'), f'data/{dataset_name}/raw_data')
    videos = {'01': 'test', '02': 'train'}

    snippets = {}

    for video in videos:
        subset = videos[video]
        get_annotations(path.join(data_path, subset), video, snippets)
        print('Video number: {:d} Snippets count: {:d}'.format(int(video), len(snippets[video])))

    # Divide into training and validation data
    train = {k: v for (k, v) in snippets.items() if '02' in k}
    test = {k: v for (k, v) in snippets.items() if '01' in k}
    all = {k: v for (k, v) in snippets.items()}

    # Save to json files
    json.dump(train, open('train.json', 'w'), indent=4, sort_keys=True)
    json.dump(test, open('test.json', 'w'), indent=4, sort_keys=True)
    json.dump(all, open('all.json', 'w'), indent=4, sort_keys=True)
    print('All videos done!')
